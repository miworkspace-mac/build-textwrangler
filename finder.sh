#!/bin/bash

NEWLOC=`curl -s http://www.barebones.com/products/textwrangler/download.html | grep -o "http.*TextWrangler.*\.dmg" -m 1`

if [ "x${NEWLOC}" != "x" ]; then
	echo "${NEWLOC}"
fi
