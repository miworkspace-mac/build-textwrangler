#!/bin/sh

ln -s /Applications/TextWrangler.app/Contents/Helpers/edit /usr/local/bin
ln -s /Applications/TextWrangler.app/Contents/Helpers/twdiff /usr/local/bin
ln -s /Applications/TextWrangler.app/Contents/Helpers/twfind /usr/local/bin

defaults write /Library/Preferences/com.lemkesoft.graphicconverter10 "SUSoftwareUpdateEnabled" 0

